﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class GameOverScreen : MonoBehaviour
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField]
        private UnityEngine.UI.Text _resultText;

        // Use this for initialization
        void Start()
        {
            _window.SetActive(false);
            GameManager.GetInstance().OnEndGame += OnEndGame;
        }

        private void OnEndGame(bool win)
        {
            _resultText.text = win ? "YOU WIN" : "YOU LOSE";
            _window.SetActive(true);
        }

        public void Replay()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }
    }
}