﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class StartScreen : MonoBehaviour
    {
        [SerializeField]
        private GameObject _window;

        // Use this for initialization
        void Start()
        {
            _window.SetActive(true);
            GameManager.GetInstance().OnStartGame += OnStartGame;
        }

        private void OnStartGame()
        {
            _window.SetActive(false);
        }
    }
}