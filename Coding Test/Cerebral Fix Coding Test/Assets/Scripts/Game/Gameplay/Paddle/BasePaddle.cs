﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class BasePaddle : MonoBehaviour
    {
        [SerializeField]
        protected float _speed = 3, _screenLimitX = .07f;

        [SerializeField]
        protected bool _smoothMovement = false;

        protected Vector3 _newPosition;
        [SerializeField]
        protected float _xLimit;

        void Start()
        {
            Vector3 newPos = Camera.main.ViewportToWorldPoint(new Vector3(_screenLimitX, 0, 0));
            _xLimit = newPos.x;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void SetPosition(Vector3 pos)
        {
            _newPosition = transform.position;
            _newPosition.x = Mathf.Clamp(pos.x, -_xLimit, _xLimit);

            if (_smoothMovement)
                transform.position = Vector3.MoveTowards(transform.position, _newPosition, _speed * Time.deltaTime);
            else
                transform.position = _newPosition;
        }
    }
}