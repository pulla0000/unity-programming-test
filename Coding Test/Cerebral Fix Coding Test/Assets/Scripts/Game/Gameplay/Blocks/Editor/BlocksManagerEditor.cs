﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Breakout
{
    [CustomEditor(typeof(BlocksManager))]
    public class BlocksManagerEditor : Editor
    {
        protected SerializedProperty _blockPrefab, _gridSize, _gridSpacing, _offset;

        BlocksManager _manager;

        protected virtual void OnEnable()
        {
            _manager = target as BlocksManager;

            _blockPrefab = serializedObject.FindProperty("_blockPrefab");
            _gridSize = serializedObject.FindProperty("_gridSize");
            _gridSpacing = serializedObject.FindProperty("_gridSpacing");
            _offset = serializedObject.FindProperty("_offset");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            bool disableButtons = _blockPrefab.objectReferenceValue == null || _gridSize.vector2Value == Vector2.zero;

            EditorGUI.BeginDisabledGroup(disableButtons);

            if (GUILayout.Button("Generate Blocks"))
            {
                for (int i = _manager.transform.childCount - 1; i > -1; i--)
                {
                    DestroyImmediate(_manager.transform.GetChild(i).gameObject);
                }

                for (int x = 0; x < _gridSize.vector2Value.x; x++)
                {
                    for (int y = 0; y < _gridSize.vector2Value.y; y++)
                    {
                        Transform t = PrefabUtility.InstantiatePrefab(_blockPrefab.objectReferenceValue) as Transform;
                        t.SetParent(_manager.transform);
                        t.name = "Block " + x + " x " + y;

                        Vector3 pos = Vector3.zero;
                        pos.x = (_gridSpacing.vector2Value.x * x) + _offset.vector2Value.x;
                        pos.y = (-_gridSpacing.vector2Value.y * y) - _offset.vector2Value.y;
                        pos.z = 0;

                        t.localPosition = pos;
                    }
                }
            }

            EditorGUI.EndDisabledGroup();

        }
    }
}