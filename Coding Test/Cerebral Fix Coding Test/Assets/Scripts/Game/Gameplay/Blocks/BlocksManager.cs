﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class BlocksManager : MonoBehaviour
    {
        [SerializeField]
        protected Transform _blockPrefab;

        [SerializeField]
        protected Vector2 _gridSize, _gridSpacing, _offset;

        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<BaseBlock>().OnDestroy += OnBlockDestroyed;
            }
        }

        private void OnBlockDestroyed()
        {
            if (transform.childCount < 2)
                GameManager.GetInstance().EndGame(true);
        }
    }
}