﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class BaseBlock : MonoBehaviour
    {
        [SerializeField]
        protected int _hitsToDestroy = 1;

        protected int _health;
        protected SpriteRenderer _renderer;

        public event System.Action OnDestroy;

        protected virtual void Start()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _health = _hitsToDestroy;
            UpdateGraphcs();
        }

        public void GotHit()
        {
            _health--;

            if (_health > 0)
                UpdateGraphcs();
            else
            {
                OnDestroy?.Invoke();
                Destroy(gameObject);
            }
        }

        protected virtual void UpdateGraphcs()
        {
            Color color = _renderer.color;
            color.a = (float)_health / (float)_hitsToDestroy;
        }
    }
}