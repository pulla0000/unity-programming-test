﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class BaseBall : MonoBehaviour
    {
        [SerializeField]
        protected float _screenOffset = .05f;

        [SerializeField]
        protected Vector2 _velocity = new Vector2(1, 5);

        protected Rigidbody2D _rigidbody2D;
        protected Vector3 _screenPos;

        // Use this for initialization
        void Start()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            GameManager.GetInstance().OnStartGame += OnStartGame;
            GameManager.GetInstance().OnEndGame += OnEndGame;
        }

        private void OnStartGame()
        {
            _rigidbody2D.velocity = new Vector2(1, 5);
        }

        private void OnEndGame(bool win)
        {
            _rigidbody2D.simulated = false;
            _rigidbody2D.velocity = Vector2.zero;
        }

        // Update is called once per frame
        void Update()
        {
            _screenPos = Camera.main.WorldToViewportPoint(transform.position);

            if (_screenPos.x < _screenOffset || _screenPos.x > 1 - _screenOffset)
                FlipX();

            if (_screenPos.y > 1 - _screenOffset)
                FlipY();

            if (_screenPos.y < 0)
                GameManager.GetInstance().EndGame(false);
        }

        protected void FlipX()
        {
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x * -1, _rigidbody2D.velocity.y);
        }

        protected void FlipY()
        {
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _rigidbody2D.velocity.y * -1);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.tag == "Block")
            {
                collision.transform.GetComponent<BaseBlock>().GotHit();
            }
        }
    }
}