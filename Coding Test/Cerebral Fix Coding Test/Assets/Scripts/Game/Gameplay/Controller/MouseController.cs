﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class MouseController : IController
    {
        protected BasePaddle _paddle;

        public bool IsControllerEnabled { get; private set; }

        public void Disable()
        {
            IsControllerEnabled = false;
        }

        public void Enable()
        {
            IsControllerEnabled = true;
        }

        public void MovePaddle()
        {
            if (!IsControllerEnabled)
                return;

            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _paddle?.SetPosition(pos);
        }

        public void SetPaddle(BasePaddle paddle)
        {
            _paddle = paddle;
        }
    }
}