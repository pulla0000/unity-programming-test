﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class TouchController : IController
    {
        protected BasePaddle _paddle;

        public bool IsControllerEnabled { get; private set; }

        public void Disable()
        {
            IsControllerEnabled = false;
        }

        public void Enable()
        {
            IsControllerEnabled = true;
        }

        public void MovePaddle()
        {
            if (!IsControllerEnabled)
                return;

            if (Input.touchCount < 1)
                return;

            Touch touch = Input.touches[0];

            Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);
            _paddle?.SetPosition(pos);
        }

        public void SetPaddle(BasePaddle paddle)
        {
            _paddle = paddle;
        }
    }
}