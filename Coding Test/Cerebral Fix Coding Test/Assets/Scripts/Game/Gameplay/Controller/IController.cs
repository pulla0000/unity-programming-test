﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public interface IController
    {
        void Enable();
        void Disable();
        void SetPaddle(BasePaddle paddle);
        void MovePaddle();
    }
}