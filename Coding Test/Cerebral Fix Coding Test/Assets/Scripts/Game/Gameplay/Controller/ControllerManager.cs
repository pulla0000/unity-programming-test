﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class ControllerManager : MonoBehaviour
    {
        private IController _controller;

        // Use this for initialization
        void Start()
        {
#if UNITY_EDITOR
            _controller = new MouseController();
#elif UNITY_ANDROID || UNITY_IOS
             _controller = new TouchController();
#endif

            GameManager.GetInstance().OnStartGame += OnStartGame;
            GameManager.GetInstance().OnEndGame += OnEndGame;

            _controller.SetPaddle(FindObjectOfType<BasePaddle>());
        }

        private void OnStartGame()
        {
            _controller?.Enable();
        }

        private void OnEndGame(bool win)
        {
            _controller?.Disable();
        }

        // Update is called once per frame
        void Update()
        {
            _controller?.MovePaddle();
        }
    }
}