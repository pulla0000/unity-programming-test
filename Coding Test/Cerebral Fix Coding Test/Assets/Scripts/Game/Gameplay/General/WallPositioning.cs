﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class WallPositioning : MonoBehaviour
    {
        [SerializeField]
        protected float _screenPositionX = 0;
        // Use this for initialization
        void Start()
        {
            Vector3 pos = transform.position;
            Vector3 newPos = Camera.main.ViewportToWorldPoint(new Vector3(_screenPositionX, 0, 0));
            pos.x = newPos.x;
            transform.position = pos;
        }      
    }
}