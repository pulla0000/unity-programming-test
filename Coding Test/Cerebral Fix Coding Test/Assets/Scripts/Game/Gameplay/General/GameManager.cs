﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakout
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager m_instance;

        public static GameManager GetInstance() { return m_instance; }

        public event System.Action OnStartGame;
        public event System.Action<bool> OnEndGame;

        private void Awake()
        {
            m_instance = this;
        }

        // Use this for initialization
        void Start()
        {

        }

        public void StartGame()
        {
            OnStartGame?.Invoke();
        }

        public void EndGame(bool win)
        {
            OnEndGame?.Invoke(win);
        }
    }
}